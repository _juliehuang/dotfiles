syntax enable

set showcmd
set number
set incsearch
set hlsearch
set autoread					" auto load files if they change on disk
set noswapfile

if &term =~ '256color'
    " Disable Background Color Erase (BCE) so that color schemes
    " work properly when Vim is used inside tmux and GNU screen.
    set t_ut=
endif
" open terminal in same directory as buffer
autocmd BufEnter * lcd %:p:h
" let &t_ut=''
" set winheight=25
set cursorline
set cursorcolumn

set showmatch
set wildmenu
set clipboard=unnamed
set splitright

set textwidth=120
set ruler						
set backspace=indent,eol,start

augroup filemaps
  au FileType python setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab encoding=utf-8
  au FileType json setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType html setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
augroup END

set ignorecase					" ignore cases when searching

filetype plugin on

call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-gitbranch'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'christoomey/vim-tmux-navigator'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-gitgutter'
Plug 'Rigellute/rigel'
Plug 'luochen1990/rainbow'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'jeetsukumaran/vim-buffergator'
Plug 'mhinz/vim-startify'
Plug 'dense-analysis/ale'
call plug#end()

map <C-n> :NERDTreeToggle<CR>   " Ctrl-n to trigger nerdtree
let g:NERDTreeShowHidden=1

map ;; :Files<CR>
imap jj <Esc>


" STARTIFY SETTINGS
" Update session automatically as you exit vim
let g:startify_session_persistence = 1
let g:startify_bookmarks = [ '~/.vimrc', '~/.zshrc' ]

" inoremap <buffer> > ></<C-x><C-o><C-y><C-o>%<CR><C-o>O  " generates closing html tags 

set noshowmode " hides status line since lightline is being used  
let g:rainbow_active = 1 "set to 0 if you want to enable it later via :RainbowToggle

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },     
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name',
      \   'filename': 'LightLineFilename'
      \ },
      \ 'colorscheme': 'deus'
      \ }

let g:ale_javascript_prettier_options = '--single-quote --trailing-comma es5'
let g:ackprg = 'ag --nogroup --nocolor --column'

set splitbelow
function! LightLineFilename()
  return expand('%')
endfunction

set termguicolors
colorscheme rigel 

let g:NERDSpaceDelims = 1       " adds a space after comment delimters
set laststatus=2
