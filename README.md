# dotfiles

A growing collection of dotfiles to set up my working environment. 

## Usage

To get started with Vim plugins, first download plug.vim 
```bash
# Vim (~/.vim/autoload)
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
Then begin a section in your `.vimrc` file with the following: 
```bash
call plug#begin('~/vim/plugged')
```
Then add the desired plugins under that. For example: 
```bash
Plug 'scrooloose/nerdtree'
```
End the section with 
```bash
call plug#end()
```

Then to install the plugins run: 
```bash
:PlugInstall
```

To setup a new computer using the brewfile, first make sure you have homebrew. 

Next, to install everything in the brewfile, run: 
```
brew bundle 
```
Homebrew will then proceed to install everything that you have specified in file. 
